package org.helio.core;

import org.helio.services.HelioPluginService;
import org.helio.spi.HelioPlugin;

public class Helio {
    static int EXIT_FAILURE = 1;

    public static void main(String[] args) {
        HelioPluginService service = HelioPluginService.getService();
        String[] wantedPluginNames = new String[] {
                "org.helio.plugins.alpha.HelioAlpha",
                "org.helio.plugins.beta.HelioBeta"
        };
        HelioPlugin[] wantedPlugins = new HelioPlugin[wantedPluginNames.length];
        for (int i = 0; i < wantedPluginNames.length; i++) {
            HelioPlugin plugin = service.getPlugin(wantedPluginNames[i]);
            if (plugin == null) {
                System.err.printf("Failed to load %s", wantedPluginNames[i]);
                System.exit(EXIT_FAILURE);
            }
            wantedPlugins[i] = plugin;
        }
        for (HelioPlugin plugin : wantedPlugins)
            plugin.configure();
    }
}
