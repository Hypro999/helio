# Helio
A small repository with a cool name to demonstrate how to use class loaders
to perform plugin management.

The style of plugin loading done here is explicit, i.e. not auto-discovery
based.

### Structure
There are 3 projects:
    1. helio-core: this contains the plugin interface (spi/HelioPlugin.java),
    the plugin loader (services/HelioPluginService.java), and the driver/main
    class (core/Helio.java).
    2. plugins/helio-alpha: sample plugin implemenetation. 
    2. plugins/helio-beta: sample plugin implemenetation. 

### Building and Running
Each project has it's own maven build file. Build all of the projects with:
```
mvn -f helio-core\pom.xml package
mvn -f plugins\helio-alpha\pom.xml package
mvn -f plugins\helio-beta\pom.xml package
```

Then set up the execution environment with:
```
mkdir runtime
mkdir runtime\plugins
cp helio-core\target\helio-core-1.0-SNAPSHOT.jar runtime\
cp plugins\helio-alpha\target\helio-alpha-1.0-SNAPSHOT.jar runtime\plugins
cp plugins\helio-beta\target\helio-beta-1.0-SNAPSHOT.jar runtime\plugins
```

Finally, execute with:
```
java -cp "runtime/helio-core-1.0-SNAPSHOT.jar;runtime/plugins/*" org.helio.core.Helio
``` 