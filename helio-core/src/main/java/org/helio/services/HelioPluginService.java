package org.helio.services;

import org.helio.spi.HelioPlugin;

public class HelioPluginService {
    private static HelioPluginService service;

    private HelioPluginService() {
        System.out.println("Creating the HelioPluginService");
    }

    public static HelioPluginService getService() {
        if (service == null) {
            service = new HelioPluginService();
        }
        return service;
    }

    public HelioPlugin getPlugin(String pluginName) {
        ClassLoader loader = HelioPluginService.class.getClassLoader();
        try {
            Class<?> plugin = loader.loadClass(pluginName);
            if (!HelioPlugin.class.isAssignableFrom(plugin)) {
                System.err.printf("%s does not implement %s\n", plugin, HelioPlugin.class);
                return null;
            }
            System.out.printf("%s implements %s\n", plugin, HelioPlugin.class);
            return (HelioPlugin) plugin.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            System.err.printf("%s: %s\n", e.getClass(), e.getMessage());
        }
        return null;
    }
}
